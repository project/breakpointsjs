<?php

/**
 * @file
 * Contains install and update functions for breakpoints_js.
 */

/**
 * Implements hook_requirements().
 */
function breakpoints_js_requirements($phase) {

  $requirements = array();

  if ($phase == 'install') {

    $t = get_t();

    // Breakpoints JS requires Enquire.js library.
    $requirements['enquire_library'] = array(
      'title' => $t('Enquire.js library'),
      'description' => $t('Breakpoints JS requires enquire.js library to be installed.'),
      'callback' => '_breakpoints_js_requirement_enquire',
      'severity' => REQUIREMENT_ERROR,
    );

    // Breakpoints JS works best with matchmedia polyfill.
    $requirements['matchmedia_polyfill'] = array(
      'title' => $t('Polyfill for matchMedia'),
      'description' => $t('Cannot find matchMedia library. Breakpoints JS uses matchMedia polyfill to address legacy browsers.'),
      'callback' => '_breakpoints_js_requirement_matchmedia',
      'severity' => REQUIREMENT_WARNING,
    );

    foreach ($requirements as &$requirement) {
      if (!empty($requirement['callback'])) {
        $requirement['callback']($requirement, $requirements);
      }
    }
  }

  return $requirements;
}

/**
 * Requirement validator for enquire.js library.
 */
function _breakpoints_js_requirement_enquire(&$requirement, &$requirements) {

  module_load_include('module', 'breakpoints_js', 'breakpoints_js');
  $library_version = breakpoints_js_libraries_enquire_version();

  if (!empty($library_version)) {
    $requirement['severity'] = REQUIREMENT_OK;
  }
}

/**
 * Requirement validator for matchMedia library.
 */
function _breakpoints_js_requirement_matchmedia(&$requirement, &$requirements) {

  module_load_include('module', 'breakpoints_js', 'breakpoints_js');
  $library_version = breakpoints_js_libraries_matchmedia_version();

  if (!empty($library_version)) {
    $requirement['severity'] = REQUIREMENT_OK;
  }
}
